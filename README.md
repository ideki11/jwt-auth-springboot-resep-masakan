# Standard CRUD Springboot Resep Masakan

# Guide Step by step membuat CRUD API Springboot

Berikut merupakan guide atau langkah-langkah bagaimana cara membuat Project Springboot secara lengkap.

## Prerequisite
- Java jdk 17.
- Maven 3.6 keatas.
- PostgreSQL 12 Keatas.
- IDE, IntelliJ, VsCode atau Eclipse.
- Pgadmin atau dbeaver.
- Postman / Thunder Client (Vs Code Extenstions).
- Database Resep Masakan.

Siapkan Environment yang dibutuhkan diatas, Jika belum install bisa cek link berikut ini : https://gitlab.com/ideki11/springboot-set-up-environment

# Guide Step by step membuat Project Springboot

Bagi yang belum membuat Struktur Project yang dibutuhkan, silahkan cek link berikut ini : https://gitlab.com/ideki11/structur-project-springboot-resep-masakan

# Guide Step by step membuat Standard CRUD API

Bagi yang belum membuat Standard CRUD API, silahkan cek link Berikut ini: https://gitlab.com/ideki11/standard-crud-springboot-resep-masakan

## YouTube Overview

[![StandardCRUDAPI](https://img.youtube.com/vi/lLtH9SgtTdQ/0.jpg)](https://www.youtube.com/watch?v=lLtH9SgtTdQ)


## Step by step JWT Auth Springboot

## Tambahkan Dependency yang diperlukan.
> Dependency
1. Tambahkan denpendency yang diperlukan pada pom.xml, seperti berikut ini:
    ```
        <dependencies>
            ...

            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt-api</artifactId>
                <version>0.11.5</version>
            </dependency>

            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt-impl</artifactId>
                <version>0.11.5</version>
                <scope>runtime</scope>
            </dependency>

            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt-jackson</artifactId>
                <version>0.11.5</version>
                <scope>runtime</scope>
            </dependency>

            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-security</artifactId>
            </dependency>

            <dependency>
                <groupId>org.springframework.security</groupId>
                <artifactId>spring-security-test</artifactId>
                <scope>test</scope>
            </dependency>

            ...
        </dependencies>
    ```

## Siapkan DTO yang dibutuhkan
>Reqeust DTO

1. Buatlah package "requests" didalam package (direktori) dtos.
2. Buatlah dto LoginRequest.java, seperti contoh berikut ini:
    ```
        import lombok.AllArgsConstructor;
        import lombok.Builder;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        public class LoginRequest {
            private String username;
            private String password;
        }
    ```
3. Buatlah dto RegisterRequest.java yang dimana terdapat beberapa ketentuan validasi, seperti contoh berikut ini:
    ```
        import jakarta.validation.constraints.NotBlank;
        import jakarta.validation.constraints.Pattern;
        import jakarta.validation.constraints.Size;
        import lombok.AllArgsConstructor;
        import lombok.Builder;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder
        public class RegisterRequest {
            @NotBlank(message = "Kolom username tidak boleh kosong")
            @Size(max = 100, message = "Format username belum sesuai.")
            @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Format username belum sesuai.")
            private String username;

            @NotBlank(message = "Kolom nama lengkap tidak boleh kosong")
            @Size(max = 255, message = "Format nama lengkap belum sesuai. (Tidak menggunakan special character dan maksimal 255 charackter)")
            @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Format nama lengkap belum sesuai. (Tidak menggunakan special character dan maksimal 255 charackter)")
            private String fullname;

            @NotBlank(message = "Kolom kata sandi tidak boleh kosong")
            @Size(max = 50, min = 6, message = "Kata sandi tidak boleh kurang dari 6 karakter")
            private String password;

            @NotBlank(message = "Kolom konfirmasi kata sandi tidak boleh kosong")
            @Size(max = 50, min = 6, message = "Kata sandi tidak boleh kurang dari 6 karakter")
            private String retypePassword;
            
            @Pattern(regexp = "^(User|Admin)$")
            private String role;
            
        }
    ```
>Response DTO

1. Buatlah package "reponses" didalam package (direktori) dtos.
2. Buatlah dto JwtResponse.java, seperti contoh berikut ini:
    ```
        import lombok.AllArgsConstructor;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        @Data
        @NoArgsConstructor
        @AllArgsConstructor
        public class JwtResponse {
            private int id;
            private String token;
            private String type = "Bearer";
            private String username;
            private String role;

            public JwtResponse(String token, int id, String username, String role) {
                this.token = token;
                this.id = id;
                this.username = username;
                this.role = role;
            }
        }
    ```

# Security
>User Details

1. Buatlah sebuah package "security".
2. Buat juga 2 buah package lainnya didalam package "security" yaitu "jwt" dan "services".
3. Pada package "services" yang ada didalam security, buatlah sebuah class `UserDetailsImplement.java`. Seperti contoh berikut ini:
    ```
        import java.util.ArrayList;
        import java.util.Collection;
        import java.util.List;

        import org.springframework.security.core.GrantedAuthority;
        import org.springframework.security.core.authority.SimpleGrantedAuthority;
        import org.springframework.security.core.userdetails.UserDetails;

        import com.example.resep.models.Users;

        import lombok.AllArgsConstructor;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public class UserDetailsImplement implements UserDetails{
            /**
            * 
            */
            private static final long serialVersionUID = 8225541649960880783L;
            private int id;
            private String username;
            private String password;
            
            private Collection<? extends GrantedAuthority> authorities;
            
            public static UserDetailsImplement build(Users user) {
                List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority(user.getRole()));
                
                return new UserDetailsImplement(
                        user.getUserId(),
                        user.getUsername(),
                        user.getPassword(),
                        authorities
                        );
            }
            @Override
            public Collection<? extends GrantedAuthority> getAuthorities() {
                
                return authorities;
            }

            @Override
            public boolean isAccountNonExpired() {
                // TODO Auto-generated method stub
                return true;
            }

            @Override
            public boolean isAccountNonLocked() {
                // TODO Auto-generated method stub
                return true;
            }

            @Override
            public boolean isCredentialsNonExpired() {
                // TODO Auto-generated method stub
                return true;
            }

            @Override
            public boolean isEnabled() {
                // TODO Auto-generated method stub
                return true;
            }

        }
    ```
    - `UserDetailsImplement` yang mengimplementasikan interface `UserDetails` dari Spring Security. Kelas ini digunakan untuk menyediakan detail user yang diperlukan untuk otentikasi dan otorisasi dalam aplikasi Spring Security.
    - Variabel Kelas:
        * `id`, `username`, dan `password` adalah atribut-atribut yang menyimpan informasi dasar tentang user seperti ID, username, dan kata sandi.
        * `authorities` adalah collection dari `GrantedAuthority`, yang mewakili otorisasi atau peran yang dimiliki oleh user. Dalam contoh ini, peran user didasarkan pada string yang disimpan dalam atribut role dari entitas Users.
    - Metode Konstruktor dan build: `public static UserDetailsImplement build(Users user)`
        * Konstruktor digunakan untuk membuat objek UserDetailsImplement dengan memberikan nilai pada atribut-atributnya.
        * Metode build adalah metode statis yang digunakan untuk membuat objek UserDetailsImplement dari objek Users. Metode ini memungkinkan konversi dari entitas Users ke UserDetailsImplement.
    - Metode getAuthorities:
        * Metode ini merupakan bagian dari interface UserDetails. Metode ini mengembalikan kumpulan otorisasi (peran) yang dimiliki oleh pengguna. Dalam contoh ini, kumpulan otorisasi diambil dari atribut authorities.
    - Metode `isAccountNonExpired`, `isAccountNonLocked`, `isCredentialsNonExpired`, dan `isEnabled`:

        * Metode-metode ini merupakan bagian dari interface UserDetails dan digunakan untuk menentukan status akun pengguna. Dalam implementasi ini, metode-metode ini mengembalikan nilai true, yang berarti akun tidak kedaluwarsa, tidak terkunci, kredensial tidak kedaluwarsa, dan diaktifkan.
    
    - Kelas `UserDetailsImplement` ini biasanya digunakan dalam konfigurasi Spring Security untuk menyediakan detail pengguna kepada mekanisme otentikasi dan otorisasi Spring Security. Dengan menyediakan implementasi interface UserDetails, aplikasi dapat mengintegrasikan data pengguna yang ada dengan mekanisme keamanan Spring Security.

4. Pada package "services" yang ada didalam security, buatlah sebuah class `UserDetailsServiceImplement.java`. Seperti contoh berikut ini:
    ```
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.security.core.userdetails.UserDetails;
        import org.springframework.security.core.userdetails.UserDetailsService;
        import org.springframework.security.core.userdetails.UsernameNotFoundException;
        import org.springframework.stereotype.Service;

        import com.example.resep.models.Users;
        import com.example.resep.repositories.UsersRepository;

        @Service
        public class UserDetailsServiceImplement implements UserDetailsService {
            @Autowired
            UsersRepository usersRepo;
            
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                Users user = usersRepo.findByUsername(username).get();
                return UserDetailsImplement.build(user);
            }

        }
    ```
> JWT

1. Pada package "jwt" yang ada didalam security, buatlah sebuah class `JwtUtils.java`. Seperti contoh berikut ini:
   ```
        import java.security.Key;
        import java.util.Date;

        import org.springframework.beans.factory.annotation.Value;
        import org.springframework.security.core.Authentication;
        import org.springframework.stereotype.Component;

        import com.example.resep.security.services.UserDetailsImplement;

        import io.jsonwebtoken.ExpiredJwtException;
        import io.jsonwebtoken.Jwts;
        import io.jsonwebtoken.MalformedJwtException;
        import io.jsonwebtoken.SignatureAlgorithm;
        import io.jsonwebtoken.UnsupportedJwtException;
        import io.jsonwebtoken.io.Decoders;
        import io.jsonwebtoken.security.Keys;
        import lombok.extern.slf4j.Slf4j;

        @Component
        @Slf4j
        public class JwtUtils {
            @Value("${resepmasakan.app.jwtSecret}")
            private String jwtSecret;
            
            @Value("${resepmasakan.app.jwtExpirationMs}")
            private int jwtExpirationMs;
            
            private Key key() {
                return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
            }
            
            public String generateJwtToken(Authentication authentication) {
                UserDetailsImplement userPrincipal = (UserDetailsImplement) authentication.getPrincipal();
                return Jwts.builder()
                        .setSubject(userPrincipal.getUsername())
                        .setIssuedAt(new Date())
                        .setExpiration(new Date(System.currentTimeMillis() + jwtExpirationMs))
                        .signWith(key(), SignatureAlgorithm.HS256)
                        .compact();
            }
            
            public String getUsernameFromJwtToken(String token) {
                return Jwts.parserBuilder().setSigningKey(key()).build()
                        .parseClaimsJws(token).getBody().getSubject();
            }
            
            public boolean validateJwtToken(String authToken) {
                try {
                Jwts.parserBuilder().setSigningKey(key()).build().parse(authToken);
                return true;
                } catch (MalformedJwtException e) {
                log.error("Invalid JWT token: {}", e.getMessage());
                } catch (ExpiredJwtException e) {
                log.error("JWT token is expired: {}", e.getMessage());
                } catch (UnsupportedJwtException e) {
                log.error("JWT token is unsupported: {}", e.getMessage());
                } catch (IllegalArgumentException e) {
                log.error("JWT claims string is empty: {}", e.getMessage());
                }

                return false;
            }
        }
   ```
   - Kodingan di atas adalah implementasi dari kelas UserDetailsService yang digunakan oleh Spring Security untuk memuat detail user berdasarkan nama pengguna (username).
   - `jwtSecret` dan `jwtExpirationMs` adalah variabel instance yang diinjeksikan dengan nilai dari file konfigurasi (`aplication.properties`) atau variabel lingkungan menggunakan anotasi @Value Spring. Pada application.properties tambahkan konfigurasi berikut ini:
        ```
        resepmasakan.app.jwtSecret= ======================ResepMasaskan=Spring===========================
        resepmasakan.app.jwtExpirationMs=86400000
        ``` 
   - `jwtSecret` adalah kunci rahasia base64-encoded yang digunakan untuk menandatangani dan memvalidasi JWTs.
   - `jwtExpirationMs` adalah waktu kedaluwarsa dalam milidetik untuk JWTs yang dihasilkan.
   - `key()` adalah metode yang mengembalikan objek Key, yang merupakan instance javax.crypto.SecretKey. Metode ini menggunakan jwtSecret untuk membuat kunci HMAC-SHA menggunakan metode Keys.hmacShaKeyFor().
   - `generateJwtToken(Authentication authentication)` adalah metode yang menghasilkan JWT token baru menggunakan UserDetailsImplement principal dari objek Authentication.
   - `getUsernameFromJwtToken(String token)` adalah metode yang mengekstrak username (subject) dari JWT token yang diberikan.
   - `validateJwtToken(String authToken)` adalah metode yang memvalidasi JWT token yang diberikan.
   - Dalam ringkasan, kelas ini menyediakan utilitas untuk menghasilkan dan memvalidasi JWT tokens menggunakan kunci rahasia dan konfigurasi waktu kedaluwarsa.

2. Pada package "jwt" yang ada didalam security, buatlah sebuah class `AuthTokenFilter.java`. Seperti contoh berikut ini:
    ```
        import java.io.IOException;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
        import org.springframework.security.core.context.SecurityContextHolder;
        import org.springframework.security.core.userdetails.UserDetails;
        import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
        import org.springframework.util.StringUtils;
        import org.springframework.web.filter.OncePerRequestFilter;

        import com.example.resep.security.services.UserDetailsServiceImplement;

        import jakarta.servlet.FilterChain;
        import jakarta.servlet.ServletException;
        import jakarta.servlet.http.HttpServletRequest;
        import jakarta.servlet.http.HttpServletResponse;
        import lombok.extern.slf4j.Slf4j;

        @Slf4j
        public class AuthTokenFilter extends OncePerRequestFilter {
            @Autowired
            private JwtUtils jwtUtils;
            
            @Autowired
            private UserDetailsServiceImplement userDetailsService;
            
            @Override
            protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
                    throws ServletException, IOException {
                try {
                    String jwt = parseJwt(request);
                    if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                        String username = jwtUtils.getUsernameFromJwtToken(jwt);

                        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                        UsernamePasswordAuthenticationToken authentication =
                                new UsernamePasswordAuthenticationToken(
                                        userDetails,
                                        null,
                                        userDetails.getAuthorities());
                        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                } catch (Exception e) {
                    log.error("Cannot set user authentication: {}", e);
                }

                filterChain.doFilter(request, response);
            }
            
            private String parseJwt(HttpServletRequest request) {
                String headerAuth = request.getHeader("Authorization");

                if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
                    return headerAuth.substring(7);
                }

                return null;
            }
        }
    ```
    - Kelas ini adalah filter Spring Security yang bertugas memvalidasi token JSON Web Token (JWT) pada setiap request HTTP.
    - `Metode doFilterInternal`: Metode ini adalah metode inti dari kelas filter ini. Metode ini dipanggil untuk setiap request HTTP dan memvalidasi token JWT yang ada pada header Authorization dari request tersebut.
    - Proses Validasi Token JWT:
        1. Pertama, metode parseJwt dipanggil untuk mengekstrak token JWT dari header Authorization pada request HTTP.
        2. Kemudian, token JWT tersebut divalidasi menggunakan metode validateJwtToken dari kelas JwtUtils. Jika token JWT tidak valid, maka akan terjadi kesalahan dan request akan dihentikan.
        3. Jika token JWT valid, maka metode getUsernameFromJwtToken dari kelas JwtUtils dipanggil untuk mengekstrak username dari token JWT tersebut.
        4. Kemudian, metode loadUserByUsername dari kelas UserDetailsServiceImplement dipanggil untuk memuat detail user berdasarkan username yang diperoleh dari token JWT.
        5. Setelah itu, objek UsernamePasswordAuthenticationToken dibuat dengan menggunakan detail user yang dimuat dan token JWT yang divalidasi.
        6. Akhirnya, metode setAuthentication dari kelas SecurityContextHolder dipanggil untuk mengeset authentication pada konteks keamanan Spring Security.
    - Tujuan dari kelas `AuthTokenFilter` ini adalah untuk memvalidasi token JWT pada setiap request HTTP dan mengeset authentication pada konteks keamanan Spring Security jika token JWT valid. Dengan demikian, aplikasi dapat memastikan bahwa hanya pengguna yang telah diotentikasi yang dapat mengakses resources yang dilindungi.

3. Pada package "jwt" yang ada didalam security, buatlah sebuah class `AuthEntryPointJwt.java`. Seperti contoh berikut ini:
    ```
        import java.io.IOException;

        import org.springframework.security.core.AuthenticationException;
        import org.springframework.security.web.AuthenticationEntryPoint;
        import org.springframework.stereotype.Component;

        import jakarta.servlet.ServletException;
        import jakarta.servlet.http.HttpServletRequest;
        import jakarta.servlet.http.HttpServletResponse;
        import lombok.extern.slf4j.Slf4j;

        @Component
        @Slf4j
        public class AuthEntryPointJwt implements AuthenticationEntryPoint {
            @Override
            public void commence(HttpServletRequest request, HttpServletResponse response,
                    AuthenticationException authException) throws IOException, ServletException {
                log.info("Cek");
                log.error("Unauthorized error: {}", authException.getMessage());
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error: Unauthorized");
                
            }
            
        }
    ```
    - Kelas ini adalah kelas yang mengimplementasikan interface AuthenticationEntryPoint Spring Security. Kelas ini digunakan untuk menangani kasus-kasus di mana autentikasi diperlukan.
    - `Metode commence` : Metode ini adalah metode yang di-overload dari kelas AuthenticationEntryPoint. Metode ini dipanggil ketika autentikasi diperlukan, tetapi tidak disediakan (not provided). Metode ini akan mengembalikan kode status HTTP 401 (Unauthorized) dan pesan kesalahan (error message).
    - Tujuan dari kelas `AuthEntryPointJwt` ini adalah untuk menangani kasus-kasus di mana autentikasi diperlukan (required), tetapi tidak disediakan (not provided). Dengan mengembalikan kode status HTTP 401 (Unauthorized), kelas ini dapat memberi tahu klien bahwa autentikasi diperlukan untuk mengakses resource yang dilindungi (protected).

# Service Users (Login & Register)
>Users Service

1. Buatlah class UsersService pada package "services".
2. Pertama beberapa source yang kita butuhkan pada class `UsersService.java` adalah seperti berikut ini:
    ```
        @Service
        @Transactional
        @Slf4j
        public class UsersService {
            @Autowired
            private UsersRepository userRepository;
            
            @Autowired
            AuthenticationManager authenticationManager;

            @Autowired
            PasswordEncoder encoder;
            
            @Autowired
            private Validator validator;

            @Autowired
            JwtUtils jwtUtils;
        
            //method register

            //method login
        }
    ```
3. Buatlah method register() untuk kebutuhan service atau logic proses Register, seperti conth berikut ini:
    ```
        public ResponseDto register(RegisterRequest registerRequest) {
            Set<ConstraintViolation<RegisterRequest>> constraintViolations = validator.validate(registerRequest);

            if(!constraintViolations.isEmpty()){
                ConstraintViolation<RegisterRequest> firstViolation = constraintViolations.iterator().next();
                String errorMessage = firstViolation.getMessage();
                return ResponseDto.builder()
                        .message(errorMessage)
                        .status("ERROR")
                        .statusCode(HttpStatus.BAD_REQUEST.value())
                        .build();
            }
            
            if(userRepository.existsByUsername(registerRequest.getUsername())){
                String errorMessage = "application.error.already-exist.user";
                return ResponseDto.builder()
                                .message(errorMessage)
                                .status("ERROR")
                                .statusCode(HttpStatus.BAD_REQUEST.value())
                                .build();
            }
            
            if (!registerRequest.getPassword().equals(registerRequest.getRetypePassword())) {
                String errorMessage = "application.error.password-not-match.user";
                return ResponseDto.builder()
                        .message(errorMessage)
                        .status("ERROR")
                        .statusCode(HttpStatus.BAD_REQUEST.value())
                        .build();
            }
            
            if(registerRequest.getPassword().length() < 6){
                String errorMessage = "application.error.password-validation.user";
                return ResponseDto.builder()
                        .message(errorMessage)
                        .status("ERROR")
                        .statusCode(HttpStatus.BAD_REQUEST.value())
                        .build();
            }
            
            //mapping data dari request ke entity
            Users user = Users.builder()
                            .username(registerRequest.getUsername())
                            .fullname(registerRequest.getFullname())
                            .password(BCrypt.hashpw(registerRequest.getPassword(), BCrypt.gensalt()))
                            .role("User")
                            .build();
            
            //save data entity
            userRepository.save(user);
            
            log.info("Received user: {}", user);
            
            String successMessage = "application.success.add.user";
            return ResponseDto.builder()
                    .message(successMessage)
                    .status("OK")
                    .statusCode(HttpStatus.OK.value())
                    .build();
        }
    ```
    - `public ResponseDto register(RegisterRequest registerRequest)` pada class UsersService.java adalah method untuk melakukan proses registrasi pengguna baru.
    - Method ini menerima parameter `RegisterRequest` registerRequest yang berisi informasi-informasi yang dibutuhkan untuk melakukan proses registrasi, seperti username, password, dan nama lengkap.
    - Method menggunakan `Validator` untuk melakukan validasi terhadap input yang diterima. Jika terdapat kesalahan validasi, maka method akan mengembalikan ResponseDto dengan status ERROR dan kode status HTTP 400 Bad Request.
    - Method akan memeriksa apakah `username` yang diberikan sudah digunakan atau belum. Jika sudah digunakan, maka method akan mengembalikan ResponseDto dengan status ERROR dan kode status HTTP 400 Bad Request.
    - Method akan memeriksa apakah `password` dan `konfirmasi password` yang diberikan sama atau tidak. Jika tidak sama, maka method akan mengembalikan ResponseDto dengan status ERROR dan kode status HTTP 400 Bad Request.
    - Method akan memeriksa `panjang password`. Jika panjang password kurang dari 6 karakter, maka method akan mengembalikan ResponseDto dengan status ERROR dan kode status HTTP 400 Bad Request.
    - Jika semua validasi dan persyaratan lainnya `terpenuhi`, maka method akan membuat objek Users baru dengan informasi yang diberikan dan menyimpan objek tersebut ke database menggunakan `userRepository`.
    - Method akan mengembalikan ResponseDto dengan status OK dan kode status HTTP 200 OK.
    - Dalam keseluruhan, method `register` ini digunakan untuk melakukan proses registrasi pengguna baru dengan validasi dan persyaratan yang diperlukan.

4. Buatlah method login() atau signIn() untuk kebutuhan service atau logic proses Register, seperti conth berikut ini:
    ```
        public ResponseDto signIn(LoginRequest loginRequest){
            try {
                Authentication authentication = authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
                
                SecurityContextHolder.getContext().setAuthentication(authentication);
                
                String jwt = jwtUtils.generateJwtToken(authentication);
                
                UserDetailsImplement userDetails = (UserDetailsImplement) authentication.getPrincipal();    
                Optional<String> roles = userDetails.getAuthorities().stream()
                    .map(item -> item.getAuthority())
                    .findFirst();
                
                log.info("Login Success!");
                
                return ResponseDto.builder()
                    .data(new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), roles.get()))
                    .message("application.success.auth.user")
                    .statusCode(statusOK.value())
                    .status(statusOK.toString())
                    .build();   
            } catch (AuthenticationException e){
                return ResponseDto.builder()
                    .message(e.toString())
                    .statusCode(HttpStatus.UNAUTHORIZED.value())
                    .status(HttpStatus.UNAUTHORIZED.toString())
                    .build();
            }catch (Exception e) {
                return ResponseDto.builder()
                    .message("application.error.internal")
                    .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    .status(HttpStatus.INTERNAL_SERVER_ERROR.toString())
                    .build();
            }
        }
    ```
    - `public ResponseDto signIn(LoginRequest loginRequest) pada class UsersService.java`: Method signIn ini digunakan untuk menangani proses login pengguna.
    - Method ini menerima parameter `LoginRequest loginRequest` yang berisi informasi-informasi yang dibutuhkan untuk proses login, seperti username dan password.
    - Method memanggil method `authenticate` dari objek `authenticationManager` dengan parameter UsernamePasswordAuthenticationToken yang berisi username dan password dari objek LoginRequest.
    - Jika autentikasi berhasil, method akan mengeset objek autentikasi pada SecurityContextHolder.
    - Method akan membuat (generate) token JWT menggunakan objek jwtUtils dan objek autentikasi.
    - Method akan mengambil detail pengguna dari objek autentikasi dan mengambil role pengguna.
    - Method akan membuat objek JwtResponse yang berisi token JWT, ID pengguna, username, dan role.
    - Method akan mengembalikan objek ResponseDto yang berisi objek JwtResponse, pesan sukses, dan kode status HTTP 200.
    - Jika autentikasi `gagal`, method akan menangkap `AuthenticationException` dan mengembalikan objek ResponseDto yang berisi pesan kesalahan dan kode status HTTP 401.
    - Jika terjadi `exception lainnya`, method akan menangkapnya dan mengembalikan objek ResponseDto yang berisi pesan kesalahan dan kode status HTTP 500.
    - Objek `ResponseDto` adalah objek kustom yang berisi data respons, pesan, dan kode status HTTP. Objek ini digunakan untuk mengembalikan format respons yang konsisten untuk semua endpoint API.
    - Objek `JwtResponse` adalah objek kustom yang berisi token JWT, ID pengguna, username, dan role. Objek ini digunakan untuk mengembalikan informasi autentikasi ke klien.

    - Objek `statusOK` adalah objek konstan yang berisi kode status HTTP 200. Objek ini digunakan untuk mengeset kode status HTTP pada objek ResponseDto.

    - Secara keseluruhan, method `signIn` mengautentikasi pengguna dengan memvalidasi username dan password, membuat token JWT, dan mengembalikan informasi autentikasi ke klien. Jika autentikasi gagal, method akan mengembalikan pesan kesalahan dan kode status HTTP 401. Jika terjadi eksepsi lainnya, method akan mengembalikan pesan kesalahan dan kode status HTTP 500.

# Controller
>UsersController
1. Buatlah class `UsersController.java` pada package "controller", seperti contoh berikut ini:
    ```
        @RestController
        @RequestMapping("/auth/user-management")
        public class UsersController {

            @Autowired
            private UsersService usersService;

            @PostMapping("/register")
            public ResponseDto register(@Valid @RequestBody RegisterRequest request){
                return usersService.register(request);
            }

            @PostMapping("/login")
            public ResponseDto signIn(@Valid @RequestBody LoginRequest loginRequest) {

                return usersService.signIn(loginRequest);
            }
        }
    ```

# Web Security Config
> WebSecurityConfig

1. Buatlah class WebSecurityConfig.java pada package "services", yang ada pada package "security".
2. Pada class WebSecurityConfig.java, lakukan konfigurasi seperti contoh berikut ini:
    ```
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;
        import org.springframework.security.authentication.AuthenticationManager;
        import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
        import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
        import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
        import org.springframework.security.config.annotation.web.builders.HttpSecurity;
        import org.springframework.security.config.http.SessionCreationPolicy;
        import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
        import org.springframework.security.crypto.password.PasswordEncoder;
        import org.springframework.security.web.SecurityFilterChain;
        import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

        import com.example.resep.security.jwt.AuthEntryPointJwt;
        import com.example.resep.security.jwt.AuthTokenFilter;
        import com.example.resep.security.services.UserDetailsImplement;
        import com.example.resep.security.services.UserDetailsServiceImplement;

        @Configuration
        @EnableMethodSecurity
        public class WebSecurityConfig {
            @Autowired
            UserDetailsServiceImplement userDetailsService;
            
            @Autowired
            private AuthEntryPointJwt unauthorizedHandler;
            
            @Bean
            public AuthTokenFilter authenticationJwtTokenFilter() {
                return new AuthTokenFilter();
            }
            
            @Bean
            public PasswordEncoder passwordEncoder() {
                return new BCryptPasswordEncoder();
            }
            
            @Bean
            public DaoAuthenticationProvider authenticationProvider() {
                DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
                
                authProvider.setUserDetailsService(userDetailsService);
                authProvider.setPasswordEncoder(passwordEncoder());
                
                return authProvider;
            }
            
            @Bean
            public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfig) throws Exception{
                return authConfig.getAuthenticationManager();
            }
            
            @Bean
            public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
                http.csrf(csrf -> csrf.disable())
                    .exceptionHandling(exception -> exception.authenticationEntryPoint(unauthorizedHandler))
                    .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                    .authorizeHttpRequests(auth -> 
                                                    auth.requestMatchers("/auth/user-management/**").permitAll()
                                                    .anyRequest().authenticated()
                            );
                
                http.authenticationProvider(authenticationProvider());

                http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
                
                return http.build();
            }
        }
    ```
    - Kodingan di atas adalah konfigurasi Spring Security yang digunakan untuk mengatur mekanisme otentikasi dan otorisasi dalam aplikasi web.
    - `Anotasi @Configuration`: Menandakan bahwa kelas ini adalah konfigurasi Spring yang akan dibaca saat aplikasi dimulai.
    - `Anotasi @EnableMethodSecurity`: Mengaktifkan dukungan untuk pengaturan keamanan berbasis metode. Ini memungkinkan penggunaan anotasi keamanan pada metode tertentu.
    - Injection dan Penggunaan Dependensi:
        * UserDetailsServiceImplement adalah implementasi UserDetailsService yang digunakan untuk mengambil detail pengguna.
        * AuthEntryPointJwt adalah kelas yang digunakan untuk menangani kasus akses yang tidak sah (unauthorized) pada aplikasi.
    - Bean Definition:
        * `authenticationJwtTokenFilter()`: Membuat bean untuk filter token otentikasi JWT.
        passwordEncoder(): Membuat bean untuk enkoder kata sandi menggunakan BCrypt.
            `passwordEncoder()`: Mendefinisikan bean untuk encoder kata sandi menggunakan BCryptPasswordEncoder yang secara otomatis akan mengenskripsi kata sandi pengguna.
        * `authenticationProvider()`: Membuat bean untuk penyedia otentikasi DAO, yang menggunakan UserDetailsService dan PasswordEncoder yang telah dikonfigurasi. Mendefinisikan bean untuk penyedia otentikasi yang menggunakan UserDetailsService dan PasswordEncoder untuk memverifikasi kredensial pengguna.
        * `authenticationManager()`: Membuat bean untuk manajer otentikasi. Ini menggunakan konfigurasi otentikasi yang disediakan oleh AuthenticationConfiguration. Mendefinisikan bean untuk pengelola otentikasi yang bertanggung jawab untuk mengatur proses otentikasi.
    - Konfigurasi HTTP Security:
        * filterChain(HttpSecurity http): Mengonfigurasi kebijakan keamanan HTTP. Di sini:
        csrf.disable(): Menonaktifkan proteksi CSRF.
            `http.csrf(csrf -> csrf.disable())`: Menonaktifkan CSRF (Cross-Site Request Forgery) karena kita menggunakan token berbasis otentikasi.
        * exceptionHandling(...): Menetapkan penanganan pengecualian untuk kasus akses yang tidak sah.
            `exceptionHandling(exception -> exception.authenticationEntryPoint(unauthorizedHandler))`: Mengatur titik masuk otentikasi yang akan menangani otentikasi yang gagal.
        * sessionManagement(...): Mengatur kebijakan manajemen sesi menjadi STATELESS, yang berarti tidak ada sesi yang disimpan di sisi server.
            `sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))`: Mengatur kebijakan sesi menjadi stateless karena kita menggunakan token JWT dan tidak menyimpan status sesi di server.
        * authorizeHttpRequests(...): Mengatur aturan otorisasi. Di sini, endpoint tertentu diizinkan tanpa otentikasi, sementara yang lainnya memerlukan otentikasi.
            `authorizeHttpRequests`(auth -> auth.requestMatchers("/auth/**").permitAll() 
            .anyRequest().authenticated()): 
            Mengatur izin akses untuk endpoint. Endpoint /auth/** diizinkan untuk diakses tanpa otentikasi, sedangkan endpoint lainnya memerlukan otentikasi.
        * authenticationProvider(authenticationProvider()): Mengatur penyedia otentikasi yang digunakan oleh aplikasi.
            `http.authenticationProvider(authenticationProvider())`: Mengatur penyedia otentikasi yang telah didefinisikan sebelumnya.
        * addFilterBefore(...): Menambahkan filter otentikasi JWT sebelum filter otentikasi standar Spring Security.
            `http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class)`: Menambahkan filter JWT sebelum filter otentikasi username dan password bawaan Spring.
    - Dengan konfigurasi ini, aplikasi menggunakan Spring Security untuk mengamankan endpoint dan menggunakan JWT untuk otentikasi berbasis token. Setiap permintaan akan diproses oleh filter JWT untuk memverifikasi token, dan jika valid, permintaan akan diizinkan untuk melanjutkan. Jika tidak valid, AuthEntryPointJwt akan menangani respons otentikasi yang gagal.