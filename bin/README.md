# Structur Project Springboot - Resep Masakan

# Guide Step by step membuat Project Springboot

Berikut merupakan guide atau langkah-langkah bagaimana cara membuat Project Springboot secara lengkap.

## Prerequisite
- Java jdk 17.
- Maven 3.6 keatas.
- PostgreSQL 12 Keatas.
- IDE, IntelliJ, VsCode atau Eclipse.
- Pgadmin atau dbeaver.
- Postman / Thunder Client (Vs Code Extenstions).
- Database Resep Masakan.

Siapkan Environment yang dibutuhkan diatas, Jika belum install bisa cek link berikut ini : https://gitlab.com/ideki11/springboot-set-up-environment


## Step by step
> Guid ini akan dimulai dari pembuatan project dari awal

> Generate Projet Springboot

1. Masuk ke link : https://start.spring.io . Buatlah sebuah Project Springboot dengan configurasi seperti gambar dibawah ini:\
![GenerateProject][def]

2. Dependency yang digunakan saat generate Project adalah:
- Spring Data JPA
- Spring Web
- Lombok
- Validation
- Spring Boot DevTools
- PostgreSQL Driver

3. Klik Generate dan download.

4. Unzip Project Resep Masakan pada drive yang diinginkan di PC/Laptop.

5. Buka Project yang sudah di unzip menggunakan IDE seperti VsCode, Ecplipse, IntelliJ IDE atau yang lainnya.

> Restore database Resep Masakan
1. Buka pgAdmin atau dBeaver.
- pgAdmin, Buka pgAdmin kemudian masukan password postgresql.
- dBeaver, Buat koneksi terlebih dahulu ke server database postgre yang ada di local kita. Seperti Berikut ini:\
    - New Database Connection
    - Pilih PostgreSQL, kemudian Next.
    - Setting connection menggunakan username dan password dari postgres sql seperti berikut ini: (Jangan lupa untuk ceklis "Show all database")\
    - Kemudian klik Test Connection. Jika setting sudah sesuai dan berhasil, maka akan menampilkan Pop up message Connection Test "Connected". Jika hasilnya gagal, maka ada kesalahan dalam setting connection nya.
    - Jika hasil test sudah Connected. Klik Finish.

2. Buka Query Tools.
3. Restore Database Resep Masakan.



[def]: img/GenerateProject.PNG

