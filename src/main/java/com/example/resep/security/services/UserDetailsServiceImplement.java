package com.example.resep.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.resep.models.Users;
import com.example.resep.repositories.UsersRepository;

@Service
public class UserDetailsServiceImplement implements UserDetailsService {
	@Autowired
	UsersRepository usersRepo;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Users user = usersRepo.findByUsername(username).get();
		return UserDetailsImplement.build(user);
	}

}
