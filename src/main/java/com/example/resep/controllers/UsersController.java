package com.example.resep.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.resep.dtos.ResponseDto;import com.example.resep.dtos.requests.LoginRequest;
import com.example.resep.dtos.requests.RegisterRequest;
import com.example.resep.services.UsersService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/auth/user-management")
public class UsersController {

    @Autowired
    private UsersService usersService;
    
    @GetMapping("/all")
    public String allAccess() {
        return "Public Content.";
    }

    @PostMapping("/register")
    public ResponseDto register(@Valid @RequestBody RegisterRequest request){
        return usersService.register(request);
    }

    @PostMapping("/login")
    public ResponseDto signIn(@Valid @RequestBody LoginRequest loginRequest) {

        return usersService.signIn(loginRequest);
    }
}
