package com.example.resep.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.resep.dtos.RecipeDto;
import com.example.resep.services.RecipeService;

@RestController
@RequestMapping("/api/recipe")
public class RecipeController {
	@Autowired
	RecipeService recipeService;
	
	//read all recipe
	@GetMapping("/read-all-recipe")
	public ResponseEntity<Object> getAllRecipe(@RequestParam(name = "size", defaultValue = "10")int size, @RequestParam(name = "page", defaultValue = "0") int page){
		return recipeService.readAllRecipe(size, page);
	}
	
	//read recipe by id
	@GetMapping("/read-all-recipe/{recipeId}")
	public ResponseEntity<Object> readAllRecipeById(@PathVariable(name = "recipeId") int recipeId){
		return recipeService.readRecipeById(recipeId);
	}
	
	//create recipe
	@PostMapping("/create-recipe")
	public ResponseEntity<Object> createRecipe(@RequestBody RecipeDto body){
		return recipeService.createRecipe(body);
	}
	
	//update recipe
	@PutMapping("/edit-recipe/{recipeId}")
	public ResponseEntity<Object> updateRecipe(@PathVariable(name = "recipeId") int recipeId, @RequestBody RecipeDto body){
		return recipeService.editRecipe(recipeId, body);
	}
	
	//delete recipe
	@DeleteMapping("/delete-recipe/{recipeId}")
	public ResponseEntity<Object> deleteRecipe(@PathVariable(name = "recipeId") int recipeId){
		return recipeService.deleteRecipe(recipeId);
	}
}
